var fs = require('fs');

var casper = require('casper').create();

function getLinks() {
    var links = document.querySelectorAll('#itemsCatalogContainer ul.tablebody li.premio');
    return Array.prototype.map.call(links, function(e) {
        return e.innerHTML;
    });
}
/**********************************************/
function getRatingsAndWrite(){
    ratings = casper.evaluate(getLinks);
    //dates = casper.evaluate(getDate);

    /*casper.echo(ratings);
    casper.echo(ratings.length + ' ratings found:');*/

    for(var i=0; i<ratings.length; i++){
        ratings[i] = ratings[i];
    }
    casper.echo(ratings);
    var content = ratings;

    content = content.join("\n");

    fs.write("personal", content, 'a'); 


    var nextLink = "ul#pager li > a";
    if (casper.visible(nextLink)) {
        casper.echo(nextLink);
        casper.thenClick(nextLink);
        casper.then(getRatingsAndWrite);
    } else {
        casper.echo("END");
    }
}
/**********************************************/


casper.start();
casper.open('https://club.personal.com.ar/club/catalog.do', {
    method: 'post'
});

casper.then(getRatingsAndWrite);
/*casper.then(function() {
    links = this.evaluate(getLinks);
});*/

casper.run(function() {
    // echo results in some pretty fashion
    //this.echo(links.length + ' links found:');
    //this.echo(' - ' + links.join('\n - ')).exit();
});