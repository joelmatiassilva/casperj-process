var express = require('express');
var app = express();
var queue = [];

//Default Handler
app.get('/',function(req,res){
res.send('Producer/Consumer Server');
});


//Producer Handler
app.get('/producer',function(req,res){
res.send('Nothing for Producer Usage /producer/{number}');
})
//Producer Handler with Input Id
app.get('/producer/:id',function(req,res){
var producerId = req.params["id"];

if(producerId == "" ||producerId.length != 0 ){
    res.send('Producer Put : '+producerId);
    queue.push(producerId);
}else{
    res.send('Nothing for Producer');
}
});

//Consumer Handler
app.get('/consumer',function(req,res){
    if(queue.length != 0){
        res.send('Consumer Get : '+queue.pop());
    }else{
        res.send('Nothing for Consumer');
    }
})

app.listen(8084);
