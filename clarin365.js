var fs = require('fs');

var casper = require('casper').create();

/*casper.start('http://365.clarin.com/?m=0&o=mv&f=&s=&d=CAPITAL+FEDERAL', function() {
    this.echo(this.getTitle());
    
	this.echo(document.querySelector('body'));
	this.echo(document.querySelector('.container'));
	
    var links = document.querySelectorAll('#content');
    Array.prototype.map.call(links, function(e) {
         //e.getAttribute('href');
         this.echo(e);
	});

});*/

function getLinks() {
    var links = document.querySelectorAll('article a');
    return Array.prototype.map.call(links, function(e) {
        return e.getAttribute('href');
    });
}

casper.start('http://365.clarin.com/', function() {
    // search for 'casperjs' from google form
    this.fill('form[action="http://365.clarin.com/"]', { d: 'CAPITAL FEDERAL',m:0 }, true);
});	

/*casper.then(function() {
      var js = this.evaluate(function() {
		return document; 
	});	
	fs.write("test", js.all[0].outerHTML, 'w')
});*/

casper.then(function() {
    links = this.evaluate(getLinks);
});

casper.run(function() {
    // echo results in some pretty fashion
    this.echo(links.length + ' links found:');
    this.echo(' - ' + links.join('\n - ')).exit();
});